import React from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';
import TodoAdd from '../todo-add';
import Login from "../login";
import TodoApi from "../../services/todo-api";

import './app.css';

class App extends React.Component {

    state = {
        todos: [],
        status: 'all',
        searchString: '',
    }
    todoApi = new TodoApi()

  onStatusChange = (statusText) => {
        this.setState({
            status: statusText
        })
  }

  onFilterByStatus = (todos, status) => {
    if (status === 'active') {
      return todos.filter((todo) => todo.done === false)
    } else if (status === 'done') {
      return todos.filter((todo) => todo.done === true)
    } else {
      return todos
    }
  }

  onSearchFilter = (todos, searchString) => {
    const result = todos.filter((todo) => todo.label.toLowerCase().includes(searchString.toLowerCase()))
    return result
  }

  onSearchChange = (searchString) => {
    this.setState({
      searchString: searchString
    })
  }


  onLoadTodos = () => {
        this.todoApi.getTodos().then(todos => {
            this.setState({
                todos: todos
            })
        })
  }

    addNewTodo = (labelText) => {
        this.todoApi.createTodo(labelText).then(data => {
            this.onLoadTodos()
        })
    }

    onToggleImportant = (id) => {
        const idx = this.state.todos.findIndex((item) => item.id === id)
        const old = this.state.todos[idx]

        const newTodo = {
            label: old.label,
            important: !old.important,
            done: old.done,
        }
        this.todoApi.updateTodo(old.id, newTodo).then(data => {
            this.onLoadTodos()
        })
    }

    onDelete = (id) => {
        this.todoApi.deleteTodo(id).then(data => {
            this.onLoadTodos()
        })
    }

    onToggleDone = (id) => {
        const idx = this.state.todos.findIndex((item) => item.id === id)
        const old = this.state.todos[idx]

        const newTodo = {
            label: old.label,
            important: old.important,
            done: !old.done,
        }
        this.todoApi.updateTodo(old.id, newTodo).then(data => {
            this.onLoadTodos()
        })
    }

  componentDidMount = () => {
      const credentials = localStorage.getItem('credentials')
      if (credentials) {
          this.onLoadTodos()
      }
  }

    render() {
        const credentials = localStorage.getItem('credentials')

        if (credentials) {
            const filteredTodos = this.onFilterByStatus(this.state.todos, this.state.status);
            const filterBySearchTodos = this.onSearchFilter(filteredTodos, this.state.searchString);

            const {todos} = this.state;
            const doneCount = todos.filter((todo) => todo.done).length;
            const todoCount = todos.length - doneCount;

            return (
                <div className="todo-app">
                    <AppHeader toDo={todoCount} done={doneCount}/>
                    <div className="top-panel d-flex">
                        <SearchPanel onSearchChange={this.onSearchChange} />
                        <ItemStatusFilter status={this.state.status} onStatusChange={this.onStatusChange}/>
                    </div>

                    <TodoList
                        onDelete = {this.onDelete}
                        onToggleImportant = {this.onToggleImportant}
                        onToggleDone = {this.onToggleDone}
                        todos={filterBySearchTodos}
                    />
                    <TodoAdd addNewTodo={this.addNewTodo} />
                </div>
            );
        } else {
            return <Login />
        }
    };
}

export default App;
