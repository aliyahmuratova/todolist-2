import React from 'react';
import './item-status-filter.css';

const ItemStatusFilter = ({status, onStatusChange}) => {
  return (
    <div className="btn-group">
      <button onClick={() => onStatusChange('all')}
              type="button"
              className={status==='all' ? "btn btn-info" : "btn btn-outline-secondary"}
      >All</button>
      <button onClick={() => onStatusChange('active')}
              type="button"
              className={status==='active' ? "btn btn-info" : "btn btn-outline-secondary"}
      >Active</button>
      <button onClick={() => onStatusChange('done')}
              type="button"
              className={status==='done' ? "btn btn-info" : "btn btn-outline-secondary"}
      >Done</button>
    </div>
  );
};

export default ItemStatusFilter;