import React from 'react';
import './todo-add.css';

class TodoAdd extends React.Component {
  state = {
    label: ''
  }

  onValueChange = (text) => {
    this.setState({
      label: text,
    })
  }

  onAddNewTodo = (event) => {
    event.preventDefault();
    this.props.addNewTodo(this.state.label)
    this.setState({label: ''})
  }

  render() {
    console.log(this.state.label)

    return (
      <div>
        <form className="item-add-form d-flex"
            onSubmit={this.onAddNewTodo}>
          <input
            onChange={(event) => this.onValueChange(event.target.value)}
            value={this.state.label}
            type='text'
            placeholder='fill the todo'
            className="form-control"
          />
          <input className="btn btn-success" type='submit' value='Add todo' />
        </form>
      </div>
    )
  }
}

export default TodoAdd;
